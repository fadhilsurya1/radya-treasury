package main

import (
	"fmt"
	"sort"
	"strconv"
	"strings"
	"sync"
)

func main() {

	// Test Case No.1
	fmt.Printf(" %+v <---- test case 1.1 ", finNum([]int{
		-1, 2,
	}))

	fmt.Printf(" %+v <---- test case 1.2 ", finNum([]int{
		-1, -7, -5,
	}))

	fmt.Printf(" %+v <---- test case 1.3 ", finNum([]int{
		1, 2, 1, 6,
	}))

	// Test Case No. 2
	resp, err := exceptNumber([]int{
		0, 1, 0,
	}, 1000, 1010)
	fmt.Printf(" %+v %+v <---- test case 2.1 ", resp, err)
	rr, r := exceptNumber([]int{
		0, 1, 0,
	}, 1259, 2601)
	fmt.Printf(" %+v %+v <---- test case 2.1 ", rr, r)

	// Test Case No.3
	fmt.Printf(" %+v <---- test case 3.1 ", findNotExist([]int{
		1, 2,
	}, []int{
		1, 3,
	}))
	fmt.Printf(" %+v <---- test case 3.2 ", findNotExist([]int{
		1, 2, 4,
	}, []int{
		2, 1,
	}))

	// No. 4
	fmt.Printf(" %+v <---- test case 4.1 ", countMap(1223334))

	// No. 5
	fmt.Printf(" %+v <---- test case 5.1 ", multiplicationGo([]int{
		1, 2, 3, 4,
	}))

}

// No. 1
func finNum(arr []int) int {
	flagPositive := false
	flagNegative := false

	for _, v := range arr {
		if v < 0 {
			flagNegative = true
		} else {
			flagPositive = true
		}
	}

	if flagNegative && flagPositive {
		return 0
	}

	sort.Ints(arr)

	if flagNegative {
		return arr[len(arr)-1]
	} else {
		return arr[0]
	}
}

// No. 2
func exceptNumber(e []int, n int, a int) (int, error) {
	var resp string
	d := fmt.Sprintf(`%+v`, n)
	f := fmt.Sprintf(`%+v`, a)
	xn := d + f
	flagCheck := false
	for i := len(xn) - 1; i > 0; i-- {
		flag := false

		for j := 0; j < len(e); j++ {
			bench := fmt.Sprintf(`%+v`, e[j])
			if string(xn[i]) == string(bench) {
				flag = true
				flagCheck = true
			}
		}

		if !flag {
			flagCheck = false
			resp += string(xn[i])
		}
	}

	if flagCheck {
		return 0, fmt.Errorf(`error with message not found`)
	}

	r, _ := strconv.Atoi(resp)

	return r, nil
}

// No. 3
func findNotExist(arr1 []int, arr2 []int) []int {
	var notExist []int

	for i := 0; i < len(arr1); i++ {
		flag := true

		for j := 0; j < len(arr2); j++ {
			if arr1[i] == arr2[j] {
				flag = false
				break
			}
		}
		if !flag {
			notExist = append(notExist, arr1[i])
		}
	}

	// sort.Sort(sort.IntSlice(notExist))
	sort.Slice(notExist, func(i, j int) bool {
		return notExist[i] > notExist[j]
	})

	return notExist
}

// No. 4
func countMap(data int) map[int]int {

	d := fmt.Sprintf(`%d`, data)
	x := make(map[int]int)
	xd := strings.Split(d, "")

	fmt.Printf(`%+v`, d)
	for _, v := range xd {
		num, _ := strconv.Atoi(v)
		_, s := x[num]
		if s {
			x[num] += 1
		} else {
			x[num] = 1
		}
	}

	return x
}

// No. 5

type multiple struct {
	Num1 int
	Num2 int
}

func multiplicationGo(arr []int) int {
	var resp int
	var xd []multiple
	var wg sync.WaitGroup
	wg.Add(len(arr))

	go func() {

		for i := 0; i < len(arr); i++ {
			defer wg.Done()
			if i%2 == 0 {
				xd = append(xd, multiple{
					Num1: arr[i],
					Num2: arr[i+1],
				})
			}
		}

		if len(arr)%2 != 0 {
			xd = append(xd, multiple{
				Num1: arr[len(arr)-1],
				Num2: arr[len(arr)-1],
			})
		}
	}()

	wg.Wait()

	for i := 0; i < len(xd); i++ {
		r := xd[i].Num1 * xd[i].Num2
		resp += r
	}

	return resp
}
